# Bootstrap 3 Theme
Bootstrap 3 Theme using Gulp and Node.js.

# Quick Overview
- Install Node.js and npm [https://nodejs.org/en/download]
- Download or clone this repository to your development environment
- Run 'npm install' to install the project dependencies.
- Open command line and change directory to the app folder [cd bootstrap3-theme].
- Run the project [npm start]

## Notes
Files are compiled into /src directory

# Demo

http://www.rascojet.com/github/bootstrap3-theme

## License
[MIT](http://opensource.org/licenses/MIT)